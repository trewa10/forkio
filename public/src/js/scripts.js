const burgerBtn = document.querySelector(".burger-btn");
const nav = document.querySelector(".nav");
const anchors = document.querySelectorAll("a.scroll-to");


burgerBtn.addEventListener("click", toggleBurger);
anchors.forEach((anchor) => anchor.addEventListener("click", addSmoothScroll));
document.body.addEventListener("click", bodyClickHandler);

function toggleBurger() {
    this.classList.toggle("burger-btn--active");
    nav.classList.toggle("nav--visible");
}

function addSmoothScroll(event) {
    event.preventDefault();
    const blockID = this.getAttribute("href");
    document.querySelector(blockID).scrollIntoView({
        behavior: "smooth",
        block: "start"
    })
}

function bodyClickHandler (event) {
    if (event.target !== burgerBtn && !nav.contains(event.target) && !burgerBtn.contains(event.target)) {
        if (nav.classList.contains("nav--visible")) {
            nav.classList.remove("nav--visible");
            burgerBtn.classList.remove("burger-btn--active");
        }
    }
}