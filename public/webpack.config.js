const path = require("path");
const glob = require("glob");
const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const copyPlugin = require("copy-webpack-plugin");
const includeHtml = require("file-include-webpack-plugin");
const miniCSS = require("mini-css-extract-plugin");
const purgeCSS = require("purgecss-webpack-plugin");
const autoPrefixer = require("autoprefixer");
const imageMin = require("imagemin-webpack-plugin").default;



module.exports = {
    performance: {
        maxAssetSize: 512000
    },
    devServer: {
        contentBase: path.resolve(__dirname, "./"),
        open: true,
        port: 5500,
        writeToDisk: true
    },
    entry: {
        main: path.resolve(__dirname, "./src/webpack-app.js"),
    },
    output: {
        path: path.resolve(__dirname, "./dist"),
        filename: "./js/scripts.min.js",
    },
    module: {
        rules: [{
            test: /\.(s*)css$/,
            use: [
                miniCSS.loader,
                {
                    loader: "css-loader",
                    options: {
                        url: false
                    }
                },
                {
                    loader: "postcss-loader",
                    options: {
                        postcssOptions: {
                            plugins: [
                                autoPrefixer({
                                    overrideBrowserslist: ["last 2 versions"]
                                })
                            ]
                        }
                    }
                },
                "sass-loader",
            ]
        },
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new includeHtml(
            {
                source: "./src/html/webpack-template",
                destination: "../",
            },
        ),
        new copyPlugin({
            patterns: [
                {from: "./src/img", to: "./img"},
            ],
        }),
        new miniCSS({
            filename: "./css/styles.min.css",
        }),
        new purgeCSS({
            paths: glob.sync("./src/**/*", {nodir: true}),
        }),
        new imageMin({
            disable: process.env.NODE_ENV.trim() !== "production",
            test: /\.(jpe?g|png|gif|svg)$/i
        })
    ]
}