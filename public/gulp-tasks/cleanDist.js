const del = require("del");

const cleanDist = (cb) => {
    del.sync("./dist/**");
    cb();
};

exports.cleanDist = cleanDist;