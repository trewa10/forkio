const {watch} = require("gulp");
const browserSync = require("./serve").browserSync;
const scriptsTask = require("./scripts").scripts;
const stylesTask = require("./styles").styles;
const imageMinTask = require("./imageMin").imageMin;
const includeHtmlTask = require("./fileInclude").includeHtml;

const watcher = () => {
    watch("./src/html/*.html", (cb) => {
        includeHtmlTask();
        browserSync.reload();
        cb();
    });
    watch("./src/js/*.js", (cb) => {
        scriptsTask();
        browserSync.reload();
        cb();
    });
    watch("./src/scss/*.scss", (cb) => {
        stylesTask();
        browserSync.reload();
        cb();
    });
    watch("./src/img/*.*", (cb) => {
        imageMinTask();
        browserSync.reload();
        cb();
    });
};

exports.watch = watcher;