const {parallel, series} = require("gulp");

const serveTask = require("./gulp-tasks/serve").serve;
const watchTask = require("./gulp-tasks/watch").watch;
const stylesTask = require("./gulp-tasks/styles");
const scriptsTask = require("./gulp-tasks/scripts");
const cleanDistTask = require("./gulp-tasks/cleanDist").cleanDist;
const prefixerTask = require("./gulp-tasks/autoPrefixer").autoPrefixer;
const purgeCSSTask = require("./gulp-tasks/purgeCSS").purgeCSS;
const imageMinTask = require("./gulp-tasks/imageMin").imageMin;
const includeHtmlTask = require("./gulp-tasks/fileInclude").includeHtml;

exports.dev = series(imageMinTask, includeHtmlTask, parallel(serveTask, watchTask, stylesTask.styles, scriptsTask.scripts));
exports.build = series(cleanDistTask, includeHtmlTask, stylesTask.stylesMin, prefixerTask, scriptsTask.scriptsMin, purgeCSSTask, imageMinTask);